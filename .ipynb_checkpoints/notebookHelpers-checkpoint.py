import pandas as pd
import matplotlib.pyplot as plt
    
def plotIt(count, title):
    df = pd.DataFrame(count.values(), index=count.keys())
    df = df.rename(columns = {list(df)[0]: title})
    df = df.sort_values(title, ascending=False)

    df.plot.bar()
    plt.show()